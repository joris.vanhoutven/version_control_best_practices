# Commits

Storing changes in the repository is a two-step process:

  1. a change (e.g., a file that was edited) is staged using
     `git add ...`, and
  2. the changes that have been changed are stored in the
     repository by a commit, i.e., `git commit`.

This means that changes to multiple files can be part of
the same commit.

r instance, you have two Python files,
`application.py` and `functions.py`.  The first is the script
you would execute, the latter contains functions that are used
in `application.py`'s code (i.e., `functions.py` is used as a
module.  You decide to change the signature of a function by adding an
additional argument.  You would have to edit both `functions.py`
and `application.py`, since only that way the code will run.

It would make a lot of sense to stage both files, i.e., both changes,
and store them in the repository with a single commit since the two
changes together are atomic.

On the other hand, it probably makes little sense to aggregate too
many changes into a single commit.

How to find the right balance, you may ask.  Don't worry, you will
develop a sense for that, and considering commit messages may help.


# Why commit messages

When you commit something using `git commit`, you will be prompted
to enter a commit message.  When you first start using a version control
system, that may seem a drag, but it is actually a very important part
of its functionality and effectiveness.

You use a version control system to document the history of your project,
but mere differences between subsequent versions don't really tell its
story.  By itself, a version control system can help you find out what
changed, when and by whom, but not why that change was made.

The motivation for making a change is likely the most important information
since this is the true documentation of the development process.

The only way to document your thought process is by supplying meaningful,
good quality commit messages.


## Commit message style

There are a number of best practices regarding commit messages.  A [good
article](https://chris.beams.io/posts/git-commit/) discussing them calls
it Seven rules for commit messages:

  1. separate subject from body with blank line
  1. limit subject to 50 characters
  1. capitalize subject line
  1. do not end subject line with period
  1. use  imperative mood in subject line
  1. wrap body at 72 characters
  1. use body to explain what & why rather than how

Now back to the question when to commit: in terms of commit messages, as
soon as you can formulate a coherent commit message.

When you notice that your commit message describes multiple changes, that
commit is too large.

However, it is very important to realize that this is pragmatics, not
religion.  It is not a sin if you do a sub-optimal commit, simply something
to notice and try to improve in the future.


## Commit message format

The seven rules cited above are pretty insistent on the format of commit
messages, and that is not a matter of aesthetics.  You can view the log of
your repository in multiple formats, e.g., displaying only the subject lines.
That way, a sizable chunk of your project's history can be displayed on a
single page.  That is why it is important to have an informative subject line,
and to limit it to 50 character to improve formatting.
