# Version control best practices

This project tries to list some best practices and
tips and tricks for using version control systems
effectively.

It will be focused on using GitLab since this is
what we intend to use in DSI.  GitLab relies on git on
the client side.

If you are not familiar with git as a version control
system, the VSC offers a training sessions on this topic.
Information and training material, including slides is
available on:
[https://gjbex.github.io/Version-control-with-git/](https://gjbex.github.io/Version-control-with-git/).

## Topics

  1. [Motivation & introduction](introduction.md)
  1. [GitLab concepts](gitlab_organization.md), groups, projects,
     repositories and all that...
  1. [Client software](clients.md)
  1. [What should I store in a repository?](repository_contents.md)
  1. [What goes into a commit and a commit message?](commits.md)
  1. Collaborating: [conflicts and branches](conflicts_and_branches.md)
  1. Is version control all we need to share code?
     [Note quite](sharing_code.md)....
