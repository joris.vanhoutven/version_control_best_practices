# How is GitLab organized?

In GitLab, there are groups, projects, repositories and
a personal namespace.


## Projects

At the basic level, you work with projects on GitLab. A
project bundles

  * a repository: this stores your files and their history;
  * issues: these are issues raised by you, your team members,
    or the users of your software; issues can be discussed;
  * merge requests: proposed changes that you can accept
    into your repository;
  * wiki: some people like to maintain a wiki to store
    information related to the project, e.g., for documentation;
  * people: you control who can see or contribute to your
    project.

All features except the repository are optional. It is up to you
to decide whether you want to use them or not.


## Personal namespace

By default, a project is created into your personal namespace.
You can view this as kind of your bespoke URL for GitLab projects.
For example, in `https://gitlab.com/geertjan.bex/gitlab_test`
(don't bother, this is a private project), you notice `geertjan.bex`,
which is Geert Jan's namespace.  All projects he creates
that are not part of a group would end up in this namespace.

This is convenient for individuals, but not for organizations, hence
GitLab has the notion of groups.

If you by accident create a new project in your personal namespace
rather than your group, or you have an existing GitLab project in
your personal namespace, no worries, it is easy to transfer the
project.


## Groups

Groups help to organize multiple projects.  They are hierarchical, so
groups can have subgroups, and so on.  Projects can be created in a group.

For DSI, a group has been created, and it is intended to contain all projects
for DSI.  To more easily organize projects, each PI can create his own
subgroup in the DSI group.  You find that group on:
[https://gitlab.com/dsi_uhasselt](https://gitlab.com/dsi_uhasselt)

Besides making it more easy to organize things, groups can also be used to control
access to subgroups and projects in a group.  That means that with the proposed
setup, a PI may have to set permissions only once on his group, and they propagate
to all subgroups and projects.

However, that will not always be appropriate, some restrictions may apply
to projects or subgroups, but that is a scenario that GitLab supports, so
subgroups and projects can have more restrictive access control than their
parent group.

At any level, a project can be private, so that it can only be accessed by its
users.

The bottom line is that as a PI, you only need to set permissions once, and take
care of exceptions to the general rule if necessary.

As an example,

  * the [DSI group](https://gitlab.com/dsi_uhasselt) is at the top level;
  * the [VSC subgroup](https://gitlab.com/dsi_uhasselt/vsc) is one level down;
  * [this repository](https://gitlab.com/dsi_uhasselt/vsc/version_control_best_practices) is in the VSC group.


## Naming and branding

You would have to pick a name for your subgroup that is descriptive
and unique.

In addition, you can add a logo for your subgroup (an avatar in GitLab-speak).
