# Introduction

"Eat your own dog food", so this "best practices" document is under
version control.

## Why version control?

Software development projects have a number of things in common,

  * things change,
  * issues need to be resolved,
  * team member collaborate.

It has certainly happened to you that you were working on some
software, everything was working fine, but suddenly things break.  Someone,
likely you, made a small change that had consequences that went unnoticed
for some time.  At that point, you scratch your head and wonder what you
changed and perhaps even why you did so.  It would be nice if it were easier
to answer those questions.

When programming, you notice some issues or opportunities for improvement,
but forget to note them, and hence they are lost for posterity.  It would be
nice to keep track of issues systematically.  If you you work in a team, you
want to discuss things, and email gets kind of messy. There should be a better
way.

And talking about teams, emailing around the latest version of source code is
such a drag, you're never sure that everyone is on the same page.  it's so
frustrating.

Sounds familiar?  No doubt.

Using a version control system (and using it well) solves these issues and
helps you be more productive.


## What is a version control system?

A version control system is essentially a repository for files that keeps
track of changes.  That means it is always easy to

  * compare with previous versions to check for changes;
  * (partially) undo changes that were made and that turn out to be
    a bad idea;
  * check when files were change, by whom, and why.

So a repository is a tool to track and document the history of your
project.  For each change you make, you can keep track of the motivation
that made you change the file.  Did you make a change to fix a bug,
improve performance, improve code style, add a feature?  You can document
that easily when you commit the changes to the repository.

If your work in a team, it is much easier to collaborate since you all
use the same repository, and can check when and why things were changed,
and whom to thank for that.

A version control system is also a great tool for discussion.  You can propose
a change, even implementing it.  Others can test and comment.  Issues can
recorded and can be assigned to specific people.  You can also comment on
issues, e.g., suggestions solutions.

Version control systems also help you to easily create web pages that
contain for instance contain the documentation of your software.
