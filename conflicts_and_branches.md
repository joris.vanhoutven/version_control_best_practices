# Conflicts and branches

## Conflicts

When you collaborate with multiple people on a project, how do you
avoid getting conflicts?  Well, that is for psychologists to answer,
but specifically for version control systems, how do you merge two
versions of the same file that each have changes that conflict with
one another?  For instance, what if two people both changed line 42?

In general, git is smart about changes that were made to he same file,
it will automatically merge those changes into a single version.  However,
this is line based, so if there are conflicting changes to the same line,
you'd have to help git.  Git will signal a merge conflict.

A merge conflict on a text file is easy to resolve. git produces a version
of the file that contains sections marking the conflict(s).  To resolve
them, the only thing you need to do is edit that file so   you have
the merged version you want. Easy.

However, it is better to avoid conflicts, and definitely to control
them.  One way to do that is by creating your own branch to make a
specific change.


## Branches

Every repository has at least one branch, 'master, but preferably more.
Branches are created from other branches, and upon creation the new
branch is identical to the original branch.  Let's call the new branch
'development'.

When you make changes to files while working in the development branch,
git keeps track of the changes as usual, but the changes are local to the
development branch, i.e., the files in the  master branch are not affected.

Once you are done making changes, it is time to propagate those changes
from the development branch into the master branch.  This is done either
by merging, or by a rebase.  Once the merge or rebase is done, the files
in both branches are identical.


## GitFlow

Branches are an indispensable tool to set up a smooth workflow.  Several
scenarios are possible but one that is widely adopted is GitFlow.  It works
well for team, but also for the lone developer.

The master branch is considered stable, i.e., it contains a version of the
files that works.  If it is a software development project, the scripts run,
the documentation is up to date.  Note that the software may contain bugs
(almost all software does) and that the documentation can be incomplete or
inaccurate (again, all documentation is).  Regardless, this is the version
you currently use in production (whatever that is).

The next stable version is prepared in a separate branch, typically called
'development'.  This is the version that you are currently working on.  If
you work in a team, the code in development should also work, although it
may contain bugs.

Still, you don't change files in the development branch, that is done in
branches that are created from development for specific tasks, e.g., adding
a feature, fixing a bug, adding tests, and so on.

These branches are short-lived since the changes you intend to make in them
are limited in scope.  Once the changes in, e.g., a feature branch are
implemented and tested, it is time to share those with the rest of the team.
A pull request is made to merge the changes into the development branch.
Once the changes are pulled into development, the features branch can be
removed.

Once the version of the files in the development breach is ready for
production, a pull request is done to merge it into the master branch.
Typically, this marks a release of the software, or the submission of an
article.

As such, using branches will not avoid conflicts, but adopting GitFlow, i.e.,
making changes in single-purpose, narrow-scope branches will go a long way
to minimize the number of conflicts.


## Tagging

Major events such as a release of your software or the submission of a
manuscript can be given a tag in git.  This makes it easy to retrieve that
point in the history of a project easily.

For instance, in the repositories for training sessions, I tag the state
of the repository for each session, so that is easy for the participants
to find the exact version of slides and examples that were used in the
training session they attended.
