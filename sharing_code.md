# Sharing code

Version control is a tool to facilitate sharing code, but
nothing more.  It helps you and others to reuse your code,
there there are other factors that are much more important.


## Code style

When code is hard to read, or hard to reuse, it is unlikely that is
going to happen.  It is good practice to pay attention to style, it
will even make your own life easier in the long run.

All programming languages have their own style guidelines, e.g.,

 * the [tidyverse style guide](https://style.tidyverse.org/) for R,
 * [PEP8](https://www.python.org/dev/peps/pep-0008/) for Python,
 * the [Google style guide](https://google.github.io/styleguide/cppguide.html)
   and the [core guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)
   for C++.

Although you may have your reservations about elements of such a style,
it is nevertheless a good idea to adhere to it.

  1. It is easier for programmers familiar with the idioms in that
     language to read and understand your code.
  1. You can use tools to format your code (semi-) automatically
     since these will be based on those style guidelines.

If you have a compelling reason to deviate from a standard guideline
and have developed your own conventions, at least make sure that those
are consistently used by all collaborators in a project.


## Comments

Code comments are good, well written code is better.  Programming is
story telling.  Variables are nouns, functions are verbs, statements
are phrases.  Well-written code needs few comments.

This is not an excuse not to write comments when you think you and
other can benefit from them, but rather to stimulate you to write good
quality code.

Also note that comments are *not* documentation.  Comments are intended
for fellow developers, not for the users of your code, not even for
someone who calls the functions you wrote.


## Documentation

If you want others to reuse your code, documentation is essential.

  1. What does a function do?
  1. How do you call the function, i.e., what is the type of the
     arguments?
  1. What is the semantics of the function arguments?  What are valid
     argument values?
  1. What does the function return, i.e., what is the return type?
  1. What is the semantics of the return value?  What can you expect
     as return values?
  1. What can go wrong, which exceptions or errors can be generated
     by this function.

Adding examples is definitely a good idea.

When documenting a script, the same kind of information has to be
provided.

Some programming languages such as Python support documentation
directory in the language syntax.  For other programming languages,
you can use tools like Sphinx or doxygen.


# Deployment

Reuse of your code will definitely increase when the code base
is easy to deploy.  Making it available on CRAN or PyPI is of course
the best, but failing that providing a make file for installation
already goes a long way.
